# Aria Incoming Email library

This repo contains scripts and library functions for managing incoming email functionality in Aria.

## Installation

* Make sure you have the mailparse extension installed ```pecl install mailparse``` (you may need to restart apache)
* ```composer require aria-php/aria-incoming-email```



## Mail Server script

The first part of this is the Email server script. This exists in ```/pipe/``` and handles the receipt of the raw email via a mail server catch-all pipe.

The script receives an email, parses out some useful information, and then fires them to Aria via a web hook. The location of which is configured in ```/pipe/config.json```

A sample config is provided.

## Incoming email webhook

The Aria part of incoming email works by listening to a webhook. An email is received by the mail server (which is likely on another machine) and this is then fired over to the incoming email webhook.

The precise structure of the endpoint is application dependant, and needs to be implemented using your application's own page routing.

Conceptually, your app needs to register an email route, using a routing pattern (pregex) and attach that to a ```callable``` function, method or closure. This callback will be passed the constructed Email object matching that route, and if you've used capture regex (e.g. ```thread-([0-9]+)@```) rather than simply matching regex patterns, you will also be passed an array of matches, which you can used to help handle the incoming email.

### Security considerations

The endpoint code performs no security checking. You should limit access to the webhook to http(s) requests from your web server only.


