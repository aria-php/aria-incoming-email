<?php

/**
 * Incoming Email MTA Pipe
 * Use this script in your MTA to handle incoming email and pass it to the internal routing endpoint.
 *
 * To use, configure your catchall handler in exim or postfix to send mail to this script.
 */

require_once(dirname(__FILE__) . '/../vendor/autoload.php');

use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;
use ARIA\mail\incoming\parsers\StreamEmailParser;
use ARIA\mail\incoming\Email;
use ARIA\mail\incoming\webhook\FormEncodedEmailDispatcher;
use ARIA\mail\incoming\webhook\Endpoint;

$endpoints = [];

// Load configuration
$config = json_decode(file_get_contents(dirname(__FILE__) . '/config.json'), true);
if (empty($config)) {
  die('Could not load "config.json", check that it exists and contains no errors');
}

// See if we've specified a single endpoint
if (!empty($config['endpoint'])) {
  $endpoints[] = new Endpoint($config['endpoint']);
}

// See if we've specified a multiple set of endpoints
if (!empty($config['endpoints']) && is_array($config['endpoints'])) {
  foreach ($config['endpoints'] as $endpoint) {
    $url = $endpoint['endpoint'];
    $condition = [];
    if (!empty($endpoint['condition'])) {
      $condition = $endpoint['condition'];
    }
    
    $endpoints[] = new Endpoint($url, $condition);
  }
}

if (empty($endpoints)) {
  die ('Endpoints are not set, either set an "endpoint" value in config.json, or list an array of "endpoints" and matching to address condition.');
}

// Simple file logging for now, but this is monolog, so we can send to DB or another server without problems.
$log = new Logger('incoming-mail-pipe');
$log->pushHandler( new ErrorLogHandler() );

// Begin processing email
try {
  
  $log->debug("Incoming Mail Pipe triggered");
  
  // Parse the incoming email
  $stream = new StreamEmailParser(fopen("php://stdin", "r"));
  $email = $stream->parse();
  
  // Check we got something
  if (empty($email)) {
    throw new \RuntimeException("No valid email could be parsed from stream");
  }
  
  // Output some info
  $log->info("Email received " . date('r') . ':');
  $log->info("\t From: " . implode(', ', (array)$email->getHeader('from')));
  $log->info("\t To: " . implode(', ', (array)$email->getHeader('to')));
  $log->info("\t CC: " . implode(', ', (array)$email->getHeader('cc')));
  $log->info("\t Subject: " . implode(', ', (array)$email->getHeader('subject')));
  
  // Send to webhook
  foreach ($endpoints as $endpoint) {
    
    try {
      $log->info("Dispatching to: " . $endpoint->getEndpointURL());
      
      if ($endpoint->matches($email)) {  
        $dispatch = new FormEncodedEmailDispatcher();
        $dispatch->setEmail($email);

        $status = 0;
        $response = $dispatch->dispatch($endpoint, $status);
        $log->debug($response);
        if ($status != 200) {
          throw new \RuntimeException("Dispatching email to {$endpoint} returned error {$status}");
        }
      } else {
        $log->info("Email doesn't match endpoint " . $endpoint);
      }
      
    } catch (\Exception $e) {
      // At this point, log errors, but continue processing other endpoints.
      $log->error($e->getMessage());
    }
    
  }
  
  
} catch (\Exception $e) {
  
  $log->error($e->getMessage());

  exit(1); // exit with an error
}

