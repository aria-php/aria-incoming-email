<?php

/**
 * Sample email endpoint.
 * This is an example web services endpoint for routing incoming email based on your
 * configured routes.
 * 
 * In a full fat system, you'd want to set this up within your application, and expose things 
 * however your application handles page routing.
 */
   

require_once(__DIR__ . '/../vendor/autoload.php');
  
use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;
use ARIA\mail\incoming\parsers\POSTRequestEmailParser;
use ARIA\mail\incoming\Email;
use ARIA\mail\incoming\Router;

// Simple file logging for now, but this is monolog, so we can send to DB or another server without problems.
$log = new Logger('incoming-mail-webhook');
$log->pushHandler( new ErrorLogHandler() );

// Begin processing email
try {
  
  $log->debug("Incoming Mail Webhook triggered");
  
  // Configure routing
  $router = new Router();
  
  
  
  
  
  
  // ... SET YOUR ROUTES HERE ...
  
  
  
  
  
  
  
  // Parse the incoming email
  $stream = new POSTRequestEmailParser();
  $email = $stream->parse();
  
  // Check we got something
  if (empty($email)) {
    throw new \RuntimeException("No valid email found in webhook");
  }
  
  // Output some info
  $log->info("Email received " . date('r') . ':');
  $log->info("\t From: " . implode(', ', (array)$email->getHeader('from')));
  $log->info("\t To: " . implode(', ', (array)$email->getHeader('to')));
  $log->info("\t CC: " . implode(', ', (array)$email->getHeader('cc')));
  $log->info("\t Subject: " . implode(', ', (array)$email->getHeader('subject')));
  
  // Attempt routing
  if ($router->route()) {
    $log->info("Processed.");
    
    echo json_encode([
      'status' => 'ok'
    ]);

  } else {
    throw new \RuntimeException('No handler found that could process any of the to/cc addresses');
  }
  
} catch (\Exception $e) {
  
  $log->error($e->getMessage());
  echo json_encode([
    'status' => 'error',
    'message' => $e->getMessage()
  ]);

  http_response_code(500);
}


