<?php

use ARIA\mail\incoming\Email;
use ARIA\mail\incoming\webhook\Endpoint;

class EndpointTest extends \PHPUnit\Framework\TestCase {
  
  public function emailEndpointProvider() {
    return [
        
        'Match Everything' => [
            (function () {
              $email = new Email();
              $email->setHeader('to', 'test-1234@someother.domain.com');
              $email->setTextBody('Example test');
              return $email;
            })(),
            ( function () {
                return new Endpoint('https://foo.com/endpoint/');
            })(),
            true
        ], 
        'Match Pattern' => [
            (function () {
              $email = new Email();
              $email->setHeader('to', 'test-1234@someother.domain.com');
              $email->setTextBody('Example test');
              return $email;
            })(),
            ( function () {
                return new Endpoint('https://foo.com/endpoint/', [
                    'to' => 'test-[0-9]+@(some|someother).domain.com'
                ]);
            })(),
            true
        ], 
        'Match Alternative Pattern' => [
            (function () {
              $email = new Email();
              $email->setHeader('to', 'test-1234@some.domain.com');
              $email->setTextBody('Example test');
              return $email;
            })(),
            ( function () {
                return new Endpoint('https://foo.com/endpoint/', [
                    'to' => 'test-[0-9]+@(some|someother).domain.com'
                ]);
            })(),
            true
        ], 
        'Don\'t Match Pattern' => [
            (function () {
              $email = new Email();
              $email->setHeader('to', 'test-1234@someother.domain.com');
              $email->setTextBody('Example test');
              return $email;
            })(),
            ( function () {
                return new Endpoint('https://foo.com/endpoint/', [
                    'to' => 'live-[0-9]+@(some|someother).domain.com'
                ]);
            })(),
            false
        ], 
        
    ];
  }
  
  /**
   * @dataProvider emailEndpointProvider
   * @param Email $email
   * @param Endpoint $endpoint
   * @param bool $expected
   */
  public function testMatch(Email $email, Endpoint $endpoint, bool $expected) {
    
    $this->assertEquals($expected, $endpoint->matches($email));
    
  }
  
}