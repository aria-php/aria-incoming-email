<?php
use ARIA\mail\incoming\Email;
  
use ARIA\mail\incoming\parsers\StreamEmailParser;

class HTMLReplyContextTest extends \PHPUnit\Framework\TestCase
{
  
  function testHTMLStripReply() {
    
    $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/reply_context.email', 'r'));
    
    $email = $emailparser->parse();
    
    $body = $email->getHTMLBody(true);
    
    // Ugly test
    $this->assertFalse(strpos($body, '<blockquote>'));
    $this->assertNotEmpty(trim($body));
  }
  
  
}