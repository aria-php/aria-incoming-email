<?php

use ARIA\mail\incoming\parsers\POSTRequestEmailParser;

class POSTRequestEmailParserTest extends \PHPUnit\Framework\TestCase
{

  private static $email;

  public static function setUpBeforeClass():void
  {
    // This is a fudge, since we've not actually got a server up and running.
    $_POST = [
      'to' => 'Marcus Povey <marcus@instruct-eric.eu>',
      'from' => 'Marcus Povey <marcus@instruct-eric.eu>',
      'body-plain' => 'Test Message'
    ];
    
    
  }


  public function testPOSTRequestEmailParser()
  {

    $emailparser = new POSTRequestEmailParser();

    self::$email = $emailparser->parse();

    $this->assertNotNull(self::$email);

  }

  public function testFrom()
  {
    $this->assertEquals('Marcus Povey <marcus@instruct-eric.eu>', self::$email->getHeader('from'));
  }

  public function testTo()
  {
    $this->assertEquals('Marcus Povey <marcus@instruct-eric.eu>', self::$email->getHeader('to'));
  }

  public function testTextBody()
  {
    $text = trim(self::$email->getTextBody()); // trim to make sure line endings aren't a problems

    $this->assertNotEmpty($text);
    $this->assertEquals('Test Message', $text);
  }

  public static function tearDownAfterClass():void
  {
    self::$email = null;
  }
}