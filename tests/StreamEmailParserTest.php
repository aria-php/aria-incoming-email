<?php

use ARIA\mail\incoming\parsers\StreamEmailParser;
use ARIA\mail\incoming\Email;

class StreamEmailParserTest extends \PHPUnit\Framework\TestCase
{


  public function simpleEmailProvider() {
    return [
      'Simple Email' => [(function() {
         $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/simple.email', 'r'));
         
         return $emailparser->parse();
      })()]  
    ];
  }
  
  public function replyEmailProvider() {
    return [
      'Reply Email' => [(function() {
         $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/reply.email', 'r'));
         
         return $emailparser->parse();
      })()]  
    ];
  }
  
  public function htmlEmailProvider() {
    return [
      'HTML Email' => [(function() {
         $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/mail_parse_test.email', 'r'));
         
         return $emailparser->parse();
      })()]  
    ];
  }
  
  public function multipleRecipientEmailProvider() {
    return [
      'Multiple To Email' => [(function() {
         $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/multiple_to.email', 'r'));
         
         return $emailparser->parse();
      })(), [
          'marcus@dushka.co.uk',
          'marcus@marcus-povey.co.uk',
          'marcus@instruct-eric.eu'
      ], 'getTo' ],
      'Multiple To Email' => [(function() {
         $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/multiple_to.email', 'r'));
         
         return $emailparser->parse();
      })(), [
          'marcus@dushka.co.uk',
          'marcus@marcus-povey.co.uk',
          'marcus@instruct-eric.eu'
      ], 'getCC' ]  
    ];
  }
  
  /**
   * @dataProvider simpleEmailProvider
   * @param Email $email
   */
  public function testFrom(Email $email)
  {
    $this->assertEquals('Marcus Povey <marcus@instruct-eric.eu>', $email->getHeader('from'));
  }

  /**
   * @dataProvider simpleEmailProvider
   * @param Email $email
   */
  public function testTo(Email $email)
  {
    $this->assertEquals('Marcus Povey <marcus@instruct-eric.eu>', $email->getHeader('to'));
  }
  
  /**
   * @dataProvider simpleEmailProvider
   * @param Email $email
   */
  public function testHeader(Email $email) 
  {
    $this->assertEquals('Test', $email->getSubject());
  }

  /**
   * @dataProvider simpleEmailProvider
   * @param Email $email
   */
  public function testTextBody(Email $email)
  {
    $text = trim($email->getTextBody()); // trim to make sure line endings aren't a problems

    $this->assertNotEmpty($text);
    $this->assertEquals('Test Message', $text);
  }
  
  /**
   * @dataProvider replyEmailProvider
   * @param Email $email
   */
  public function testTextBodyWithoutReply(Email $email)
  {
    $text = trim($email->getTextBody(true)); // trim to make sure line endings aren't a problems

    $this->assertNotEmpty($text);
    $this->assertEquals("This is a reply", $text);
  }

  /**
   * @dataProvider htmlEmailProvider
   * @param Email $email
   */
  public function testHTMLBody(Email $email)
  {
    $this->assertNotNull($email);

    $html = $email->getHTMLBody();
    $this->assertNotEmpty($html);
    
    $text = trim($email->getTextBody()); // trim to make sure line endings aren't a problems 
    $this->assertNotEmpty($text); 
  }

  /**
   * @dataProvider multipleRecipientEmailProvider
   * @param Email $email
   * @param array $addresses
   * @param string $getter
   */
  public function testMultipleRecipients(Email $email, array $addresses, string $getter) {
    
    $recipients = $email->$getter();
    
    $this->assertIsArray($recipients);
    $this->assertNotEmpty($recipients);
    
    foreach ($addresses as $address) {
      
      
      $this->assertNotEmpty(array_filter($recipients, function($var) use ($address) {
        
        // Parse out addresses int
        $var = mailparse_rfc822_parse_addresses($var);
        $var = current($var); // Something weird was going on with the array, figure it out later.
        $var = $var['address'];
        
        return $var == $address;
        
      }), "$address is not in " . print_r($recipients, true));
    }
  }
  
}