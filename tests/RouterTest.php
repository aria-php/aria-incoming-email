<?php

  
  use ARIA\mail\incoming\{Email, Router};
  
  class RouterTest extends \PHPUnit\Framework\TestCase {
    
    private static $router;

    public static function setUpBeforeClass():void
    {
      self::$router = new Router();
    }
    
    
    public function addRouteProvider() {
      return [
          'The Route not called' => ['test2-([0-9]+)@example.com', [$this, 'emailRoute2']],
          'First route' => ['test-([0-9]+)@example.com', [$this, 'emailRoute']],
          'Any domain route' => ['test3-([0-9]+)@', [$this, 'emailRouteAnyDomain']]
      ];
    }
    
    /**
     * 
     * @param string $route
     * @param callable $handler
     * @dataProvider addRouteProvider
     */
    public function testAddRoute(string $route, callable $handler) {
      
      $result = self::$router->addRoute($route, $handler);
      $this->assertTrue($result);
      
    }

    public function testBadRoute() {
      $email = new Email();
      $email->setHeader('to', 'testbad-1234@example.com');
      $email->setTextBody('Example test');
      
      $this->assertFalse(self::$router->route($email));
      
    }
    
    public function goodRoutesProvider() {
      return [
          
          /**
           * A little explanation as to what's going on here.
           * We create test data from a lambda function (closure), which would normally return 
           * a reference to itself. In this instance we don't want this, instead we want to populate the 
           * array with the _result_ of the closure.
           * As of PHP 7 you can do this by calling the function straight away using the extra bracket syntax,
           * i.e. (lambda)(), which is ( function() { ... } )()
           */
          
          
          'Standard route' => [(function () {
            $email = new Email();
            $email->setHeader('to', 'test-1234@example.com');
            $email->setTextBody('Example test');
            return $email;
          })()],
                  
          'Domain Agnostic Route' => [(function () {
            $email = new Email();
            $email->setHeader('to', 'test3-1234@someother.domain.com');
            $email->setTextBody('Example test');
            return $email;
          })()]
      ];
    }
    
    /**
     * Test all the good routes
     * @param Email $email
     * @dataProvider goodRoutesProvider
     */
    public function testGoodRoutes(Email $email) {
      
      $this->assertTrue(self::$router->route($email));
      
    }
    
    public function emailRoute(Email $email, array $matches = []) {
      $this->assertEquals($email->getHeader('to'), 'test-1234@example.com');
      $this->assertEquals($matches[1], '1234');
      
      return true;
    }
    
    public function emailRoute2(Email $email, array $matches = []) {
      
      throw new \RuntimeException("emailRoute2 has been triggered, which means routing went a bit wrong");
      
      $this->assertEquals($email->getHeader('to'), 'test2-1234@example.com');
      $this->assertEquals($matches[1], '1234');
      
      return true;
    }
    
    public function emailRouteAnyDomain(Email $email, array $matches = []) {
      $this->assertEquals($email->getHeader('to'), 'test3-1234@someother.domain.com');
      $this->assertEquals($matches[1], '1234');
      
      return true;
    }
  }