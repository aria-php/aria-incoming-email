<?php

use ARIA\mail\incoming\Email;

/**
 * Tests the basics of the email parser system.
 */
class EmailTest extends \PHPUnit\Framework\TestCase
{
  public function testBasicHeaders()
  {

    $test = new Email();

    // Single header
    $test->setHeader('foo', 'bar');
    $this->assertEquals($test->getHeader('foo'), 'bar');

    // Multiple headers
    $test->setHeader('foo', 'bar2');
    $this->assertEquals($test->getHeader('foo')[0], 'bar');
    $this->assertEquals($test->getHeader('foo')[1], 'bar2');

    // Adding as array
    $test->setHeader('foo', ['bar3','bar4']);
    $this->assertEquals($test->getHeader('foo')[0], 'bar');
    $this->assertEquals($test->getHeader('foo')[1], 'bar2');
    $this->assertEquals($test->getHeader('foo')[2], 'bar3');
    $this->assertEquals($test->getHeader('foo')[3], 'bar4');
  }

}