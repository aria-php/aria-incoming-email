<?php

use ARIA\mail\incoming\Email;
use ARIA\mail\incoming\webhook\FormEncodedEmailDispatcher;  
use ARIA\mail\incoming\parsers\StreamEmailParser;
use ARIA\mail\incoming\webhook\Endpoint;
  
class FormEncodedEmailDispatcherTest extends \PHPUnit\Framework\TestCase
{
  const endpoint = 'https://httpbin.org/post';

  public function dispatchProvider() {
    
    return [
        'Simple email' => [
            (function() {
              $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/simple.email', 'r'));
              
              return $emailparser->parse();
            })()
        ],
        'HTML email' => [
            (function() {
              $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/mail_parse_test.email', 'r'));
              
              return $emailparser->parse();
            })()
        ],
        'Attachment test' => [
            (function() {
              $emailparser = new StreamEmailParser(fopen(dirname(__FILE__) . '/data/attachments.email', 'r'));
              
              return $emailparser->parse();
            })()
        ]
    ];
  }
  
  /**
   * @dataProvider dispatchProvider
   * @param Email $email
   */
  public function testDispatch(Email $email) {
    
    // Dispatch message to endpoint
    $dispatcher = new FormEncodedEmailDispatcher();
    $dispatcher->setEmail($email);
    
    $status = 0;
    $response = $dispatcher->dispatch(new Endpoint(self::endpoint), $status);
    
    // Basic assertions
    $this->assertNotEmpty($response);
    $this->assertEquals($status, 200);
    
    // Parse what we sent
    $json = json_decode($response, true);
    
    // Reflect to get the fields to validate
    $reflected = new ReflectionClass('ARIA\mail\incoming\webhook\FormEncodedEmailDispatcher');
    $validationFields = $reflected->getProperty('toSend');
    $validationFields->setAccessible(true);
    
    $fields = $validationFields->getValue(new FormEncodedEmailDispatcher());
    $foundfields = false;
    
    foreach ($fields as $header) {
     
      if (!in_array($header, [
        'in-reply-to'
      ])) {
        if (!empty($json['form'][$header])) {
          $foundfields = true;
          $this->assertEquals($json['form'][$header], $email->getHeader($header), "Header is $header");
        }
      }
    
    }
    
    $this->assertTrue($foundfields);
    
    // Attachments, if there are any
    if (!empty($json['files'])) {
      $foundfiles = 0;
      foreach ($json['files'] as $filename => $file) {
          $foundfiles++;
      }

      $this->assertEquals($foundfiles, 1);
    }
    
    
  }

}
  