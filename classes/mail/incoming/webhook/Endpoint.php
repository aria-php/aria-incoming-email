<?php

 
namespace ARIA\mail\incoming\webhook;

use ARIA\mail\incoming\Email;

class Endpoint {
  
  /**
   * Endpoint URL
   * @var type 
   */
  private $endpoint;
  
  /**
   * Regex for matching email to (default is everything)
   * @var type 
   */
  private $conditions = [
      'to' => '.*'
  ];
  
  /**
   * Construct a new endpoint
   * @param string $endpoint Endpoint URL
   * @param array $conditions Optional match conditions
   * @throws RuntimeException
   */
  public function __construct(string $endpoint, array $conditions = []) {
    
    if (!filter_var($endpoint, FILTER_VALIDATE_URL))
    {
      throw new \RuntimeException('Endpoint value is not a URL');
    }
    
    $this->endpoint = $endpoint;
    
    if (!empty($conditions))
      $this->conditions = $conditions;
  }
  
  /**
   * Retrieve the URL of the endpoint
   * @return string
   */
  public function getEndpointURL() : string {
    return $this->endpoint;
  }
  
  /**
   * Does email match conditions.
   * @param Email $email
   * @return bool
   */
  public function matches(Email $email):bool {
    
    $return = true;
    
    foreach ($this->conditions as $header => $regex) {
      
      if (!preg_match("/$regex/", $email->getHeader($header))) {
        $return = false;
      }
      
    }
    
    return $return;
  }
  
  /**
   * Return a printable representation of this endpoint
   * @return string
   */
  public function __toString() {
    return $this->endpoint . ' ' . json_encode($this->conditions);
  }
}

