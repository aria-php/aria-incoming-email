<?php
  
namespace ARIA\mail\incoming\webhook;

use ARIA\mail\incoming\Email;

/**
 * Dispatch an email to the webhook.
 */
abstract class EmailDispatcher 
{
  /**
   * Email to dispatch.
   */
  private $email;
  
  /**
   * Get the bound email object
   * @return \ARIA\mail\incoming\Email
   */ 
  public function getEmail(): ? Email {
    return $this->email;
  }
  
  /**
   * Bind an email object with this dispatcher.
   */
  public function setEmail(Email $email) {
    $this->email = $email;
  }
  
  /**
   * Dispatch an email to the endpoint.
   * @param endpoint $endpoint The url of the webhook endpoint
   * @param int $status If provided, the status code of the request will be placed here
   * @return returns the response from the server
   */
  abstract public function dispatch(Endpoint $endpoint, int &$status = 0) : string;
  
}  
  