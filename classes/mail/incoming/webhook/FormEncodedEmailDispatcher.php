<?php
  
namespace ARIA\mail\incoming\webhook;

use ARIA\mail\incoming\Email; 

/**
 * Send an email and fields as an application/x-www-form-urlencoded 
 * (or multipart/form-data if there are attachments) request to a url.
 */
class FormEncodedEmailDispatcher extends EmailDispatcher 
{
  /**
   * Headers to send (we don't want to send absolutely everything)
   */
  private $toSend = [
    'to',
    'from',
    'cc',
    'subject',
    'message-id',
    'in-reply-to',
    'references',
    'date',
    'thread-index',
    'content-type',
    'errors-to', // deprecated, but still sometimes works
    'return-path'
  ];
  
  public function dispatch(Endpoint $endpoint, int &$status = 0) : string
  {
    $endpoint = $endpoint->getEndpointURL();
    $client = new \GuzzleHttp\Client();
    
    $email = $this->getEmail();
    if (empty($email)) {
      throw new \RuntimeException("No email available to dispatch");
    }
    
    // Form posting
    $form = [];
    
    // Map body
    $form['body-html'] = $email->getHTMLBody();
    $form['body-plain'] = $email->getTextBody();
    
    // Create form headers
    foreach ($this->toSend as $header) {
      $form[$header] = $email->getHeader($header);
    }
    
    $request = [];
    
    if ($attachments = $email->getAttachments()) {
        foreach ($attachments as $attachment) {
            $request['multipart'][] = [
                'name' => 'attachments[]',
                'contents' => $attachment->getStream(),
                'filename' => $attachment->getFilename(),
            ];
        }
        
        // Add form params as multipart
        foreach ($form as $key => $value) {
            $request['multipart'][] = [
                'name' => $key,
                'contents' => $value
            ];
        }
        
    } else {
        $request = [
            'form_params' => $form
        ];
    }
    
    
    // Send the request
    $response = $client->request('POST', $endpoint, $request);
    
    // Set status code 
    $status = $response->getStatusCode();
    
    // Return the response data
    return $response->getBody();
  }
  
}