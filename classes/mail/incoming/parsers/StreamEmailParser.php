<?php

namespace ARIA\mail\incoming\parsers;

use ARIA\mail\incoming\Email;

/**
 * Create an Email object from a stream of the raw email, headers and all.
 */
class StreamEmailParser extends EmailParser
{
  /**
   * Mailparse object
   */
  private $mailparse;

  /**
   * Construct a new Email object from a stream resource of a RAW email (including headers).
   * @param stream $stream A stream as opened by fopen, e.g. fopen("php://stdin", "r")
   */
  public function __construct($stream)
  {
    if (empty($stream))
      throw new \RuntimeException('Stream resource is empty');

    $this->mailparse = new \PhpMimeMailParser\Parser();
    $this->mailparse->setStream($stream);
  }

  /**
   * Parse raw email into internal Email object
   */
  public function parse()
  {

    $email = new Email();

    // Get the body
    $email->setTextBody($this->mailparse->getMessageBody('text'));
    $email->setHTMLBody($this->mailparse->getMessageBody('html'));
    
    // Correct for emails that are ONLY html
    if (empty($email->getTextBody())) {
      $email->setTextBody(\Soundasleep\Html2Text::convert($email->getHTMLBody(), [
        'ignore_errors' => true
      ]));
    }

    // Get the headers
    $headers = $this->mailparse->getHeaders();

    foreach ($headers as $header => $value) {
      $email->setHeader($header, $value);
    }

    // Get any attachments
    $attachments = $this->mailparse->getAttachments();
    if (!empty($attachments)) {
      $email->setAttachments($attachments);
    }

    return $email;
  }
}

