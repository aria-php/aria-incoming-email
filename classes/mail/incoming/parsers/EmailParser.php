<?php

namespace ARIA\mail\incoming\parsers;

/**
 * Define a way of parsing various sources into an Email object
 */
abstract class EmailParser
{
  /**
   * Parse email contents into an Email object
   */
  abstract public function parse();
}

