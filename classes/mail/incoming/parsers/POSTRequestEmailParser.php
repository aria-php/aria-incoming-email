<?php
  
namespace ARIA\mail\incoming\parsers;

use ARIA\mail\incoming\Email;

/**
 * Create a new Email object from $POSTed values.
 */
class POSTRequestEmailParser extends EmailParser {
  
  public function parse()
  {
    
    $email = new Email();

    // Get the body
    if (isset($_POST['body-plain'])) 
      $email->setTextBody($_POST['body-plain']);
    
    if (isset($_POST['body-html']))
      $email->setHTMLBody($_POST['body-html']);

    // Get the headers
    foreach ($_POST as $header => $value) {
      if (!in_array($header, [ // Assume that everything that is not body text is a header
          'body-plain',
          'body-html'
      ])) { 
        $email->setHeader($header, $value);
      }
    }

    // Get any attachments
    if (!empty($_POST['attachments'])) {
      $email->setAttachments($_POST['attachments']);
    }

    return $email;
  }
}