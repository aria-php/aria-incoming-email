<?php

namespace ARIA\mail\incoming;

use PHPHtmlParser\Dom;

/**
 * Incoming Email class.
 * This class represents an email as it arrives from the MTA, or at the webhook.
 */
class Email implements \JsonSerializable
{
  /**
   * Key => value array of headers.
   */
  private $headers = [];

  /**
   * Plain text body, where available.
   */
  private $bodyText;

  /**
   * Rich text body
   */
  private $bodyHTML;

  /**
   * Attachments
   */
  private $attachments = [];

  /**
   * Set the text body of an email.
   */
  public function setTextBody($text)
  {
    $this->bodyText = $text;
  }

  /**
   * Set the html body of an email
   */
  public function setHTMLBody($html)
  {
    $this->bodyHTML = $html;
  }

  /**
   * Set the attachments
   */
  public function setAttachments(array $attachments = [])
  {
    $this->attachments = $attachments;
  }

  /**
   * Set a header
   * @param string $header The header name
   * @param string|array $value The value of the header
   */
  public function setHeader($header, $value)
  {
    $header = strtolower($header);
    
    // Does this header already exist?
    if (isset($this->headers[$header])) {

      $existing = $this->headers[$header];

      // If it exists, but isn't already a multiple entry, simplify things below by converting it to an array
      if (!is_array($existing))
        $existing = [$existing];

      // If $value isn't an array already, make it so, simplify things below
      if (!is_array($value))
        $value = [$value];

      // Add headers
      $this->headers[$header] = array_merge($existing, $value);

    }

    // Not an existing header
    else {
      $this->headers[$header] = $value;
    }

  }

  /**
   * Retrieve a header value.
   * If multiple values with the same name are found, an array is returned
   * @return string|array|false on error
   */
  public function getHeader($header)
  {
    if (isset($this->headers[$header]))
      return $this->headers[$header];

    return false;
  }

  /**
   * Retrieve the plain text portion of an email.
   * @param bool $strip_reply Strip the reply from the email and return only the message body
   * @return text body
   */
  public function getTextBody(bool $strip_reply = false)
  {
    $body = $this->bodyText;
    
    if ($strip_reply) {
      $body = \EmailReplyParser\EmailReplyParser::parseReply($body);
    }
    
    return trim($body);
  }

  /**
   * Retrieve the HTML portion of an email.
   * @param bool $strip_reply Make *best effort* to strip the reply from the email and return only the message body
   * @return html body
   */
  public function getHTMLBody(bool $strip_reply = false)
  {
    $body = $this->bodyHTML;
    
    if ($strip_reply && $body) {
      
      // Strip reply contexts (ht: https://b-alidra.com/strip-quoted-text-from-html-emails/)
      $contexts_to_remove = [
        'blockquote', // Standard quote block tag
        'div.moz-cite-prefix', // Thunderbird
        'div.gmail_extra', 'div.gmail_quote', // Gmail
        'div.yahoo_quoted' // Yahoo
      ];
      
      $dom = new Dom;
      $dom->load($body);
      foreach ($contexts_to_remove as $el) {
          $founds = $dom->find($el)->toArray();
          foreach ($founds as $f) {
              $f->delete();
              unset($f);
          }
      }
      
      $body = $dom->root->innerHtml();
      
      // This is a a very very simplistic and probably fragile way of stripping the html reply. 
      // Not a lot we can do about that, since even GitHub gave up on trying to make this work everywhere.
      //$body = preg_replace('/\<blockquote.*\<\/blockquote\>/is', '', $body);
      
      // Now, lets sanitise the html
      $config = \HTMLPurifier_Config::createDefault();
      $config->set('CSS.AllowedProperties', []);
      if (defined('CACHE_PATH')) {
        $config->set('Cache.SerializerPath', CACHE_PATH);
      }
      $purifier = new \HTMLPurifier($config);
      
      $body = $purifier->purify($body);
    }
    
    return trim($body);
  }

  /**
   * Return the attachments
   */
  public function getAttachments()
  {
    return $this->attachments;
  }
  
  /**
   * Get To field recipients
   * 
   * @return array|null
   */
  public function getTo() : ?array
  {
    if ($to = $this->getHeader('to')) {
      return array_map('trim', explode(',', $to));
    }
    
    return null;
  }
  
  /**
   * Get CC field recipients
   * 
   * @return array|null
   */
  public function getCC() : ?array
  {
    if ($cc = $this->getHeader('cc')) {
      return array_map('trim', explode(',', $cc));
    }
    
    return null;
  }
  
  /**
   * Return an email's subject
   * @return string|null
   */
  public function getSubject() : ? string {
    if ($subject = $this->getHeader('subject')) {
      if (is_array($subject)) {
        $subject = $subject[0]; // If munged into an array, return the first version
      }
      
      return $subject;
    }
    
    return null;
  }
  
  /**
   * Return an email's from field
   * @return string|null
   */
  public function getFrom() : ? string {
    if ($from = $this->getHeader('from')) {
      if (is_array($from)) {
        $from = $from[0]; // If munged into an array, return the first version
      }
      
      return $from;
    }
    
    return null;
  }

  public function jsonSerialize() {
    return [
        'headers' => $this->headers,
        'bodyText' => $this->bodyText,
        'bodyHTML' => $this->bodyHTML,
    ];
  }

}

