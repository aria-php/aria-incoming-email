<?php

namespace ARIA\mail\incoming;

/**
 * Handle the routing of incoming
 */
class Router
{
  /**
   * Routing table of 'regex' => 'callable'
   */
  private $routing = [];
  
  /**
   * Create a routing table.
   * @params array $routing Optionally specify a complete routing table during creation.
   */
  public function __construct(array $routing = []) {
    
    // If we've specified a routing table at this point, attempt to add it.
    if (!empty($routing)) {
      foreach ($routing as $pattern => $handler) {
        $this->addRoute($pattern, $handler);
      }
    }
  }

  /**
   * Add a routing handler for an email address.
   * Accepts an email address in the form of $regex@domain.com (perl compatible) so, for example
   * message_[0-9]+@domain.com will match any of the following message_56@domain.com message_2452@.... but not message_23f@...
   * 
   * You can also specify () to capture the match pattern, in which case these are also passed to your handler.
   *
   * @param string $pattern Email address pattern to match
   * @param callable $handler Handler with the definition of function handler(Email $email, array $matches = []), which returns true if it successfully handles the address
   * @return bool
   */
  public function addRoute(string $pattern, callable $handler)
  {

    list($regex, $domain) = explode('@', trim($pattern));

    if (!is_callable($handler)) {
      throw new \RuntimeException('Handler function is not callable');
    }

    $this->routing["{$regex}@{$domain}"] = $handler;

    return true;
  }

  /**
   * Route an email to its registered handler.
   * This method routes an email to its registered handler, passing it an Email object, and the regex matches
   * according to the registered pattern.
   *
   * @returns bool
   * @throws RuntimeException
   */
  public function route(Email $email)
  {

    // Get email address from email
    $to = $email->getTo();
    if (empty($to)) {
      throw new \RuntimeException('No "to" address could be found in the email.');
    }
    if (!is_array($to)) {
      $to = [$to];
    }
    
    // May also want to route based on CC
    $cc = $email->getCC();
    if ($cc && !is_array($cc)) {
      $cc = [$cc];
    }
    
    // For each address attached to this email see if we can find a mapping (only one per email)
    $addresses = array_merge($to, (array)$cc);
    foreach ($addresses as $to) {
    
      // Sanitise and find the actual routing address, in the case of John Doe <john@example.com> format address
      $to = mailparse_rfc822_parse_addresses($to);
      $to = current($to); // Something weird was going on with the array, figure it out later.
      $to = $to['address'];
      
      // See if we have a handler for this email
      foreach ($this->routing as $pattern => $handler) {

        // Normalise email address pattern
        list($regex, $domain) = explode('@', $pattern);
        $domain = str_replace('.', '\.', $domain);
  
        // See if we can handle this pattern
        $matches = null;
        if (preg_match("/{$regex}\@{$domain}/", $to, $matches)) {
  
          // Call the handler
          if (call_user_func_array($handler, [
              $email,  // Email
              $matches // If your pattern contains regex capture, results are placed here.
          ]))
            return true; // We've now handled this email, return success and quit
        }
      }
    }

    return false; // If we got here, then the email was not handled.
  }
}


